﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Carps.Helper;
using Dapper;
using Dapper.Contrib;
using Dapper.Contrib.Extensions;
using Carps.Definitions;

namespace Carps.Model
{
    /// <summary>
    /// Class represents the Carps database model in respect to auto-allocation
    /// </summary>
    public class CarpsData
    {
        public IDbConnection db;
        public IEnumerable<CarpsTask> Tasks;
        public IEnumerable<CarpsJob> Jobs;
        public IEnumerable<CarpsAutoAllocationTask> AutoAllocationTasks;
        public IEnumerable<CarpsOperative> Operatives;
        public IEnumerable<CarpsDevice> Devices;

        private IEnumerable<CarpsPendingTaskQuery> _pendingTasks;
        private IEnumerable<CarpsOperative> _operativeRecentLocations;

        public CarpsData()
        {
            db = new SqlConnection(INISettings.ConnectionString);
            db.Open();

            // fill data fields
            Tasks = db.Query<CarpsTask>(
                "SELECT * FROM tblTask"
                );
            Jobs = db.Query<CarpsJob>(
                "SELECT * FROM tblJob"
                );
            AutoAllocationTasks = db.Query<CarpsAutoAllocationTask>(
                "SELECT * FROM tblAutoAllocationTasks"
                );
            Operatives = db.Query<CarpsOperative>(
                "SELECT * FROM tblOperative"
                );
            Devices = db.Query<CarpsDevice>(
                "SELECT * FROM tblDevice"
                );
        }

        /// <summary>
        /// Gets a list of pending tasks based on the supplied filter
        /// </summary>
        /// <param name="filter">SQL filter string e.g. "(WHERE 0=0)"</param>
        /// <returns></returns>
        public IEnumerable<CarpsPendingTaskQuery> FilterPendingTasks(String filter)
        {
            // Sanitize filter expressions
            filter = filter.Replace("[CurrentTime]", "SUBSTRING(convert(varchar, GETDATE(),108),1,5)");
            filter = filter.Replace("[CurrentDayOfWeek]", "DATENAME(dw,GETDATE())");
            // Check if Auto Allocation table has values
            if (AutoAllocationTasks != null && AutoAllocationTasks.GetEnumerator().MoveNext())
            {
                // Search for tasks based on filter
                _pendingTasks = db.Query<CarpsPendingTaskQuery>(
                "SELECT Task, JobNumber, RadioString, FromID, ISNULL(TeamJobNumbers, 0) AS TeamJobNumbers FROM " +
                "(SELECT tbljob.JobNumber, tbljob.RadioString, tblCategory.Category, tblJob.BillingCode, tblPriority.Priority, tblPriority.Sort, tblLocation.Location AS BookedByLocation, tblTask.Task, tbllocation_1.Location AS FromLocation, tbllocation_2.Location AS ToLocation, tbljob.Comment, tblJob.WhenDueStamp, CASE WHEN tbljob.Flags=1 THEN 'PreBooked' WHEN tbljob.Flags=2 THEN 'Recurring' ELSE 'Ad Hoc' END AS BookingType, DATENAME(dw,tbljob.WhenDueStamp) As DueDayOfWeek, SUBSTRING(convert(varchar, tbljob.WhenDueStamp,108),1,5) AS DueTime, CASE WHEN tbljob.MitigationNoted=0 THEN 'False' ELSE 'True' END AS MitigationNoted, CASE WHEN tbljob.AllocationFailure=0 THEN 'False' ELSE 'True' END AS AllocationFailure, CASE WHEN tbljob.TimeoutWarning=0 THEN 'False' ELSE 'True' END AS TimeoutWarning, CASE WHEN tbljob.HasNotes=0 THEN 'False' ELSE 'True' END AS HasNotes, CASE WHEN tbljob.HasAttachments=0 THEN 'False' ELSE 'True' END AS HasAttachments, tblJob.TeamJobNumbers, tblJob.FromID " +
                "FROM tblTask WITH (NOLOCK) RIGHT OUTER JOIN tblLocation AS tbllocation_1 WITH (NOLOCK) RIGHT OUTER JOIN tblLocation AS tbllocation_2 WITH (NOLOCK) RIGHT OUTER JOIN tblCategory WITH (NOLOCK) RIGHT OUTER JOIN tbljob WITH (NOLOCK) LEFT OUTER JOIN tblLocation WITH (NOLOCK) ON tbljob.BookedByID = tblLocation.LocationID ON tblCategory.CategoryID = tbljob.CategoryID LEFT OUTER JOIN tblOperativeAllocations ON tbljob.JobNumber = tblOperativeAllocations.JobNumber ON tbllocation_2.LocationID = tbljob.ToID ON tbllocation_1.LocationID = tbljob.FromID ON tblTask.TaskID = tbljob.TaskID LEFT OUTER JOIN tblPriority WITH (NOLOCK) ON tbljob.PriorityID = tblPriority.PriorityID LEFT OUTER JOIN " +
                "(SELECT JobNumber, OperativeID, MAX(TimeStamp) AS TimeStamp FROM tblJobTimestampLog WHERE (TimeStampType = 2) GROUP BY JobNumber, OperativeID) AS qLastAllocation LEFT OUTER JOIN tblOperative WITH (NOLOCK) ON qLastAllocation.OperativeID = tblOperative.OperativeID ON tbljob.JobNumber = qLastAllocation.JobNumber " +
                "WHERE (tbljob.Sort = 0) AND (tbljob.JobStateID = 1) AND (tbljob.WhenDueStamp - tbljob.Reminder <= GETDATE()+0.00003) AND (tblOperativeAllocations.AllocationID IS NULL)) AS q " +
                "WHERE " + filter + " " +
                "ORDER BY q.Sort, q.WhenDueStamp"
                );
                return _pendingTasks;
            }
            Console.WriteLine("Error: No records found in tblAutoAllocationTasks");
            return null;
        }

        public JobTimeStampType OperativeLastJobTimeStampType(int operativeID)
        {
            string sqlQuery =
                    "SELECT LastJobTimeStampType FROM tblOperative " +
                    "INNER JOIN(SELECT tl1.OperativeID as JobTimeStampOpId, tl1.JobNumber as LastJobNumber, tl1.TimeStampType as LastJobTimeStampType, tl1.JobTimeStampID as LastJobTimeStampID FROM tblJobTimestampLog as tl1 " +
                    "INNER JOIN(SELECT OperativeID, MAX(TimeStamp) AS TimeStamp, MAX(JobTimeStampID) as JobTimeStampID FROM tblJobTimestampLog WITH(NOLOCK) " +
                    "WHERE(TimeStampType IN(1, 2, 3, 4, 5, 6, 7, 8)) GROUP BY OperativeID) as tl2 ON tl1.OperativeID = tl2.OperativeID AND tl1.JobTimeStampID = tl2.JobTimeStampID) as CurrentJob ON tblOperative.OperativeID = CurrentJob.JobTimeStampOpId" +
                    "AND OperativeID = " + operativeID;
            JobTimeStampType jobTimeStampType = (JobTimeStampType)db.ExecuteScalar<int>(sqlQuery);
            return jobTimeStampType;
        }

        public int GetOperativeTaskCount(int operativeID)
        {
            string sqlQuery =
                "SELECT OperativeID, COUNT(AllocationID) as TaskCount FROM tblOperativeAllocations WHERE OperativeID = " + operativeID + " GROUP BY OperativeID";
            return db.ExecuteScalar<int>(sqlQuery);
        }

        public int HighestDurationOperativeId()
        {
            string sqlQuery = 
                "SELECT TOP 1 OperativeID From tblOperative WHERE active = 1 ORDER BY StatusTimeStamp DESC";
            return db.ExecuteScalar<int>(sqlQuery);
        }

        public IEnumerable<CarpsDevice> GetOperativeDevice(int opertaiveId)
        {
            string sqlQuery = "SELECT * FROM tblDevice WHERE DeviceID = " + opertaiveId;
            return db.Query<CarpsDevice>(sqlQuery);
        }
    }

    /// <summary>
    /// Represents tblOperatives
    /// </summary>
    [Table("tblOperatives")]
    public class CarpsOperative : IAutoAllocatable
    {
        [Key]
        public int OperativeID { get; set; }
        public string OperativeName { get; set; }
        public int OpStatus { get; set; }
        public DateTime StatusTimeStamp;
        public int DeviceID { get; set; }
        public DateTime? RTTTimestamp;
        public int Ack { get; set; }
        public int LastLocID { get; set; }
        public int DestLocID { get; set; }
        public string LastMsg { get; set; }
        public string LastTX { get; set; }
        public int RetryCount { get; set; }
        public string Timeout { get; set; }
        public string Comment { get; set; }
        public string Note { get; set; }
        public string SkinName { get; set; }
        public int Presence { get; set; }
        public int LoneWorkerTaskID { get; set; }
        public int AutoAllocationTaskID { get; set; }
        public string Password { get; set; }
        public int UserType { get; set; }
        public string Tag { get; set; }
        public int Active { get; set; }
        public DateTime? InsertStamp;
        public DateTime? UpdateStamp = DateTime.Now;
        public int UpdateDeviceID { get; set; }

        public void AllocateTo(int JobId)
        {
            throw new NotImplementedException();
        }

        public void DeAllocateFrom(int JobId)
        {
            throw new NotImplementedException();
        }

        public string Filter { get; set; }
    }

    /// <summary>
    /// Represents tblAutoAllocationTasks
    /// </summary>
    [Table("tblAutoAllocationTasks")]
    public class CarpsAutoAllocationTask
    {
        [Key]
        public int AutoAllocationID { get; set; }
        public string AutoAllocationName { get; set; }
        public string TaskFilter { get; set; }
    }

    /// <summary>
    /// Represents tblTask
    /// </summary>
    [Table("tblTask")]
    public class CarpsTask
    {
        [Key]
        public int TaskID { get; set; }
        public string Task { get; set; }
        public string Abrev { get; set; }
        public int Active { get; set; }
        public string Tag { get; set; }
        public DateTime? InsertStamp;
        public DateTime? UpdateStamp = DateTime.Now;
        public int UpdateDeviceID { get; set; }
        public int PriorityID { get; set; }
        public int PatientNameField { get; set; }
        public int PatientURField { get; set; }
        public int PatientDOBField { get; set; }
        public int PatientBednumberField { get; set; }
        public int PatientSexField { get; set; }
        public int HasRisksField { get; set; }
        public int Field1 { get; set; }
        public int Field2 { get; set; }
        public int Field3 { get; set; }
        public int Field4 { get; set; }
        public int ToLocationField { get; set; }
        public int PatientLocationField { get; set; }
        public bool IsObsTask { get; set; }
    }

    /// <summary>
    /// Represents tblJob
    /// </summary>
    [Table("tblJob")]
    public class CarpsJob
    {
        [Key]
        public int JobNumber { get; set; }
        public string Terminal { get; set; }
        public int JobStateID { get; set; }
        public int CategoryID { get; set; }
        public int BillingCodeID { get; set; }
        public string BillingCode { get; set; }
        public int PriorityID { get; set; }
        public int BookedByID { get; set; }
        public int TaskID { get; set; }
        public int FromID { get; set; }
        public int ToID { get; set; }
        public DateTime? WhenDueStamp;
        public string Reminder { get; set; }
        public string Comment { get; set; }
        public string RadioString { get; set; }
        public int Sort { get; set; }
        public DateTime? Timeout;
        public int DelayDuration { get; set; }
        public int Flags { get; set; }
        public int HasNotes { get; set; }
        public int HasAttachments { get; set; }
        public int HasRisks { get; set; }
        public string PatientName { get; set; }
        public string PatientUR { get; set; }
        public string PatientDOB { get; set; }
        public string PatientBedNumber { get; set; }
        public string RequestorName { get; set; }
        public string RequestorRole { get; set; }
        public string RequestorContact { get; set; }
        public int AllocationFailure { get; set; }
        public int ResponseFailure { get; set; }
        public int CompletionFailure { get; set; }
        public int MitigationNoted { get; set; }
        public int TimeoutWarning { get; set; }
        public string TeamJobNumbers { get; set; }
        public DateTime? InsertStamp;
        public DateTime? UpdateStamp = DateTime.Now;
        public int UpdateDeviceID { get; set; }
        public int KeyPadTaskID { get; set; }
        public string ExternalReference { get; set; }
        public string PatientLocation { get; set; }
        public string AdmissionNumber { get; set; }
        public int BedLocationId { get; set; }
    }

    /// <summary>
    /// Represents tblDevice
    /// </summary>
    [Table("tblDevice")]
    public class CarpsDevice
    {
        [Key]
        public int DeviceID { get; set; }
        public int DeviceTypeID { get; set; }
        public string DeviceNumber { get; set; }
        public string Channel { get; set; }
        public string ExtendedCallNumber { get; set; }
        public string DefaultVoice { get; set; }
        public string DefaultStatus { get; set; }
        public int Active { get; set; }
        public DateTime? InsertStamp;
        public DateTime? UpdatedStamp = DateTime.Now;
        public int UpdateDeviceID { get; set; }
        public string WifiBSSID { get; set; }
        public string WifiRSSI { get; set; }
        public DateTime? WifiTimeStamp;
        public string GcmRegistration { get; set; }
        public string IpAddress { get; set; }
    }

    /// <summary>
    /// Represents a query returning pending tasks
    /// </summary>
    public class CarpsPendingTaskQuery
    {
        public string Task { get; set; }
        public string JobNumber { get; set; }
        public string RadioString { get; set; }
        public int FromID { get; set; }
        public string TeamJobNumbers { get; set; }
    }

    /// <summary>
    /// Represents a query returning last job details for operative
    /// </summary>
    public class CarpsOperativeLastJobQuery : CarpsOperative
    {
        public int JobTimeStampOpId { get; set; }
        public string LastJobJumber { get; set; }
        public int LastJobTimeStamp { get; set; }
        public int LastJobTimeStampID { get; set; }
    }
}
