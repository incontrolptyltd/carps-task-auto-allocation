﻿namespace Carps.Definitions
{
    public interface IAllocatable
    {
        void AllocateTo(int JobId);
        void DeAllocateFrom (int JobId);
    }

    public interface IAutoAllocatable : IAllocatable
    {
        string Filter { get; set; }
    }

    public enum CarpsEntityType: int { Operative, User, Equipment }

    public enum LocationType: int { From, To }


    public enum JobTimeStampType
    {
        TaskLogged = 1,
        TaskAllocated = 2,
        TaskOnRoute = 3,
        TaskOnJob = 4,
        TaskCompleted = 5,
        TaskCancelled = 6,
        TaskPaused = 7,
        TaskResumed = 8,
        TaskEdited = 9,
        TaskDue = 10,
        TaskHandover = 11,
        TaskAllocation = 12,
        TaskQueued = 13,
        DeviceAck = 14
    }

    public enum JobState
    {
        Pending = 1,
        Active = 2,
        Completed = 3,
        OnHold = 4,
        Allocated = 5
    }
}