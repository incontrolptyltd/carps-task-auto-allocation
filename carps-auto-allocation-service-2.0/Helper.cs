﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using IniParser;
using IniParser.Model;

namespace Carps.Helper
{
    /// <summary>
    /// Retrieves and stores data from the Carps.ini file
    /// </summary>
    static class INISettings
    {
        public static string ConnectionString
        {
            get
            {
                var connectionString = Data["Server"]["ConnectionString"].ToString();
                return connectionString;
            }
        }
        public static string RadioHost
        {
            get
            {
                var radioHostString = Data["Radio"]["RadioHost"].ToString();
                return radioHostString;
            }
        }
        public static bool SingleAllocationOnly
        {
            get
            {
                bool isSingleAllocationOnly = bool.Parse(Data["System"]["SingleAllocationOnly"].ToString());
                return isSingleAllocationOnly;
            }
        }
        public static string GcmUrl
        {
            get
            {
                var gcmUrlString = Data["GCM"]["GcmUrl"].ToString();
                return gcmUrlString;
            }
        }
        public static string WebProxy
        {
            get
            {
                var webProxyString = Data["GCM"]["WebProxy"].ToString();
                return webProxyString;
            }
        }
        public static string WebProxyUserName
        {
            get
            {
                var webProxyUserNameString = Data["GCM"]["WebProxyUserName"].ToString();
                return webProxyUserNameString;
            }
        }
        public static string WebProxyPassword
        {
            get
            {
                var webProxyPasswordString = Data["GCM"]["WebProxyPassword"].ToString();
                return webProxyPasswordString;
            }
        }
        public static int GroupID
        {
            get
            {
                var groupId = int.Parse(Data["Server"]["GroupID"].ToString());
                return groupId;
            }
        }
        private static string AppPath { get; }
        public static string IniPath { get; }

        private static FileIniDataParser File = new FileIniDataParser();
        private static IniData Data;

        static INISettings()
        {
            AppPath = GetAppPath();
            IniPath = AppPath + "Carps.ini";
            Data = File.ReadFile(IniPath);
        }

        static string GetAppPath()
        {
            var appPath = Assembly.GetExecutingAssembly().Location
                .Replace(Assembly.GetExecutingAssembly().GetName().Name + ".exe", "");
            return appPath;
        }
    }

    /// <summary>
    /// Class handles logging events in the windows event logger.
    /// </summary>
    static class CarpsEventLogger
    {

    }
}
