﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Topshelf;
using System.Data.SqlClient;
using System.Data.Sql;
using Carps.Definitions;
using Carps.Helper;
using System.Data;
using System.Runtime.InteropServices;
using System.Net.Sockets;
using System.Diagnostics;
using System.Timers;
using Carps.Model;
using Carps.Helper;
using Carps.Definitions;
using Dapper.Contrib.Extensions;
using System.Diagnostics;
using Dapper;

namespace Carps.CarpsAutoAllocation
{
    class Program
    {
        static void Main(string[] args)
        {
            ConfigureService.Configure();
            Console.ReadLine();
        }
    }

    /// <summary>
    /// Configuration Service class, sets up windows service.
    /// </summary>
    internal static class ConfigureService
    {
        // Boiler plate code for TopShelf library:
        internal static void Configure()
        {
            HostFactory.Run(configure =>
            {
                configure.Service<CarpsAutoAllocationService>(service =>
                {
                    service.ConstructUsing(s => new CarpsAutoAllocationService());
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                });
                //Setup Account that window service use to run.  
                configure.RunAsLocalSystem();
                configure.SetInstanceName("CarpsService");
                configure.SetServiceName(": CarpsAutoAllocationService");
                configure.SetDisplayName(": Carps Auto Allocation");
                configure.SetDescription(": Carps auto allocation service. Used to allocate operatives to tasks based on configuration parameters.");
            });
        }
    }
    internal class CarpsAutoAllocationService
    {
        public CarpsData carpsDataModel;
        public RadioHost carpsRadioHost;
        public static SqlConnection SqlConn;
        readonly Timer _timer;

        /// <summary>
        /// Constructor: sets up new timer and respective event handling
        /// </summary>
        public CarpsAutoAllocationService()
        {
            carpsRadioHost = new RadioHost();
            carpsDataModel = new CarpsData();

            // Connect to radios?

            // Set up timer
            _timer = new Timer(5000) { AutoReset = true};
            _timer.Elapsed += (sender, EventArgs) =>
            {
                Console.WriteLine("------------------------------------------------------------------------------------------------");
                Console.WriteLine("CarpsAutoAllocationService : iterating through list auto allocation filters");
                Console.WriteLine("------------------------------------------------------------------------------------------------");
                // Check what auto allocation filters exist and iterate
                foreach (var Allocation in carpsDataModel.AutoAllocationTasks)
                {
                    Console.WriteLine("CarpsAutoAllocationService - found pending tasks using filter (id = {0}, filter = {1})", Allocation.AutoAllocationID, Allocation.TaskFilter);
                    IEnumerable<CarpsPendingTaskQuery> pendingTasks = carpsDataModel.FilterPendingTasks(Allocation.TaskFilter);
                    if (pendingTasks.Any())
                    {
                        // Loop through pending tasks that were found using the filter
                        Console.WriteLine("CarpsAutoAllocationService -- Iterating through pending tasks ({0})", pendingTasks.Count());
                        foreach(var pendingTask in pendingTasks)
                        {
                            Console.WriteLine("CarpsAutoAllocationService --- Checking if task (jobnumber = {0}) has any operatives in the same area", pendingTask.JobNumber);
                            // Check for operatives in the area
                            foreach (var operative in carpsDataModel.Operatives)
                            {
                                if (operative.LastLocID == pendingTask.FromID)
                                {
                                    Console.WriteLine("CarpsAutoAllocationService ---- Operative (ID = {0}) found in same area as job start location", operative.OperativeID);
                                    // Check if operative is allocated to a job
                                    if(carpsDataModel.OperativeLastJobTimeStampType(operative.OperativeID) != JobTimeStampType.TaskAllocated)
                                    {
                                        if ((INISettings.SingleAllocationOnly &&
                                             carpsDataModel.GetOperativeTaskCount(operative.OperativeID) > 0) ||
                                            !INISettings.SingleAllocationOnly)
                                        {
                                            Console.WriteLine("CarpsAutoAllocationService ----- Assigning Operative to Job");
                                            AllocateOperativeToJob(operative.OperativeID, pendingTask.JobNumber);
                                            // Get operative device info and send radio message
                                            IEnumerable<CarpsDevice> devices = from d in carpsDataModel.Devices
                                                where d.DeviceID == operative.DeviceID
                                                select d;
                                            CarpsDevice carpsDevice = devices.FirstOrDefault();
                                            carpsRadioHost.SendToDevice(carpsDevice.DeviceID, carpsDevice.Channel, carpsDevice.DeviceNumber, pendingTask.RadioString);
                                        }
                                    }
                                }
                                // Compare task's starting location with operative's destination location
                                else
                                {
                                    if (operative.DestLocID == pendingTask.FromID)
                                    {
                                        // Check for single allocation setting and task count of 0 for operative, or single allocation off
                                        if ((INISettings.SingleAllocationOnly &&
                                             carpsDataModel.GetOperativeTaskCount(operative.OperativeID) > 0) ||
                                            !INISettings.SingleAllocationOnly)
                                        {
                                            Console.WriteLine("CarpsAutoAllocationService ---- Found operative with same destinationId as job start location");
                                            AllocateOperativeToJob(operative.OperativeID, pendingTask.JobNumber);
                                            // Get operative device info and send radio message
                                            IEnumerable<CarpsDevice> devices = from d in carpsDataModel.Devices
                                                where d.DeviceID == operative.DeviceID
                                                select d;
                                            CarpsDevice carpsDevice = devices.FirstOrDefault();
                                            carpsRadioHost.SendToDevice(carpsDevice.DeviceID, carpsDevice.Channel, carpsDevice.DeviceNumber, pendingTask.RadioString);
                                        }
                                    }
                                    // else assign to longest wait time
                                    else
                                    {
                                        if ((INISettings.SingleAllocationOnly &&
                                             carpsDataModel.GetOperativeTaskCount(operative.OperativeID) > 0) ||
                                            !INISettings.SingleAllocationOnly)
                                        {
                                            Console.WriteLine("CarpsAutoAllocationService ---- Assigning operative with longest wait time");
                                            AllocateOperativeToJob(carpsDataModel.HighestDurationOperativeId(),pendingTask.JobNumber);
                                            // Get operative device info and send radio message
                                            IEnumerable<CarpsDevice> devices = from d in carpsDataModel.Devices
                                                            where d.DeviceID == operative.DeviceID
                                                            select d;
                                            CarpsDevice carpsDevice = devices.FirstOrDefault();
                                            carpsRadioHost.SendToDevice(carpsDevice.DeviceID, carpsDevice.Channel, carpsDevice.DeviceNumber, pendingTask.RadioString);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }    
            };
        }

        public void Start()
        {
            try
            {
                // start timer
                _timer.Start();
                Console.WriteLine("Service Started!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public void Stop()
        {
            // stop timer
            _timer.Stop();
            Console.WriteLine("Service Stopped!");
        }

        public void AllocateOperativeToJob(int operativeId, string jobNumber)
        {
            SqlConn.Execute("UPDATE tblJob SET JobStateID = 5, Sort = 1 WHERE JobNumber = @jobNumber",new {jobNumber});

            SqlConn.Execute("INSERT INTO tbljobtimestamplog (JobNumber, TimeStampType, TimeStamp, OperativeID, ControllerID, Manual, InsertStamp) " +
                            "VALUES (@jobNumber, 2, GETDATE(), @operativeId, 0, 0, GETDATE())", new {operativeId, jobNumber});

            SqlConn.Execute("INSERT INTO tblOperativeAllocations(OperativeID, AllocationType, JobNumber, Timeout, Sort) " +
                            "VALUES (@operativeId, 0, @jobNumber, '00:00', 0", new {operativeId, jobNumber});
        }
    }
    
    internal class RadioHost
    {
        static TcpClient Socket;

        public bool ConnectedToRadios { get; set; }

        public RadioHost()
        {
            try
            {
                ConnectToSocket();
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Carps.Allocation.Service",
                    "RadioHost failed to construct Exception: " + e.Message,
                    EventLogEntryType.Error);
                Console.WriteLine("Failed to connect to radios, check Event Log entries for more details");
            }
        }

        private void ConnectToSocket()
        {
            try
            {
                Socket = new TcpClient() { ReceiveTimeout = 2000, SendTimeout = 2000 };
                IAsyncResult result = Socket.BeginConnect(INISettings.RadioHost, 9004, null,null);
                if (!Socket.Connected) {
                    Socket.Close();
                    ConnectedToRadios = false;
                    throw new Exception("Can't connect to Carps Communication");
                }
                else
                {
                    ConnectedToRadios = true;
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Carps.Allocation.Service", "RadioHost.ConnectToSocket threw Exception: " + ex.Message, EventLogEntryType.Error);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        private void DisconnectFromSocket()
        {
            try
            {
                if (Socket.Connected)
                {
                    Socket.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public void SendToDevice(int deviceId, string channel, string deviceNumber, string radioString)
        {
            if (ConnectedToRadios)
            {
                try
                {
                    if (deviceId != 19)
                    {
                        Socket.Client.Send(Encoding.UTF8.GetBytes("ENQUEUE" + channel + "|" + deviceNumber + "|" + "DCALL" + "|" + radioString + "|" + "0" + "|\r\n"));
                    }
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("Carps.Allocation.Service", "Timer_Tick(SendToRadio) Exception: " + ex.Message, EventLogEntryType.Error);
                    Console.WriteLine(ex);
                    throw;
                }
            }
            else
            {
                EventLog.WriteEntry("Carps.Allocation.Service", "RadioHost.SendToDevice gave Warning: Radio host is not connected, check if the correct ip is set in the .ini file, or that the host is available or accessible", EventLogEntryType.Error);
            }
        }
    }
}
